﻿using exam.Core;
using exam.Migrations;
using exam.Models;
using exam.Models.DTO;
using Microsoft.EntityFrameworkCore;

namespace exam.Services
{
    public interface IOrderService
    {
        public Task<ApiResponse<OrderCreateDTO>> CreateOrder(OrderCreateDTO orderCreateDTO);

        public Task<ApiResponse<OrderModelRes>> GetListOrder(string textSearch, int pageNum, int pageSize);
    }

    public class OrderService : IOrderService
    {
        private readonly MigrationDataContext _dbService;

        public OrderService(MigrationDataContext dbService)
        {
            _dbService = dbService;
        }

        public async Task<ApiResponse<OrderCreateDTO>> CreateOrder(OrderCreateDTO orderCreateDTO)
        {
            var res = new ApiResponse<OrderCreateDTO>
            {
                Data = orderCreateDTO,
                StatusCode = StatusCodeResponse.SUCCESS,
            };
            try
            {
                var productInfor = await _dbService.Product.FirstOrDefaultAsync(x => x.ProductId == new Guid(orderCreateDTO.Product));
                if (productInfor == null)
                {
                    res.StatusCode = StatusCodeResponse.PRODUCT_INVALID;
                    return res;
                }
                if (productInfor.Quantity < 1)
                {
                    res.StatusCode = StatusCodeResponse.QUANTITY_PRODUCT_INVALID;
                    return res;
                }

                OrderModel orderModel = new()
                {
                    Amount = Convert.ToDouble(orderCreateDTO.Amount),
                    Customer = orderCreateDTO.Customer,
                    OrderDate = Convert.ToDateTime(orderCreateDTO.OrderDate),
                    Product = orderCreateDTO.Product
                };
                _dbService.Order.Add(orderModel);
                productInfor.Quantity -= 1;
                _dbService.Product.Attach(productInfor);
                _dbService.Entry(productInfor).State = EntityState.Modified;
                await _dbService.SaveChangesAsync();
                return res;
            }
            catch (Exception ex)
            {
                res.StatusCode = StatusCodeResponse.EXEPTION;
                res.Message = ex.Message;
                return res;
            }
        }

        public async Task<ApiResponse<OrderModelRes>> GetListOrder(string textSearch, int pageNum, int pageSize)
        {
            var res = new ApiResponse<OrderModelRes>
            {
                Data = new OrderModelRes(),
                Message = string.Empty,
                StatusCode = StatusCodeResponse.SUCCESS
            };
            try
            {
                res.Data.ListProduct = await (from order in _dbService.Order
                                              join customer in _dbService.Customer on order.Customer equals customer.CustomerId.ToString()
                                              join product in _dbService.Product on order.Product equals product.ProductId.ToString()
                                              join category in _dbService.Category on product.Category equals category.CategoryId.ToString()
                                              orderby order.OrderDate descending
                                              select new OrderDTO
                                              {
                                                  Amount = ConvertUtility.FormatAmoutSpecialCcy(order.Amount, "VND"),
                                                  CategoryName = category.Name,
                                                  CustomerName = customer.Name,
                                                  OrderDate = order.OrderDate.ToString("dd/MM/yyyy"),
                                                  OrderId = order.OrderId.ToString(),
                                                  ProductName = product.Name
                                              }).ToListAsync();
                if (!string.IsNullOrEmpty(textSearch))
                {
                    res.Data.ListProduct = res.Data.ListProduct
                        .Where(x => ConvertUtility.UnicodeToKoDau(x.CategoryName).ToLower().Contains(ConvertUtility.UnicodeToKoDau(textSearch).ToLower())).ToList();
                    if (res.Data.ListProduct.Count == 0)
                    {
                        return res;
                    }
                }
                var totalRecord = res.Data.ListProduct.Count;
                res.Data.ListProduct = res.Data.ListProduct
                                      .Skip((pageNum - 1) * pageSize)
                                      .Take(pageSize)
                                      .ToList();
                res.Data.TotalPage = totalRecord % pageSize > 0 ? (totalRecord / pageSize) + 1 : totalRecord / pageSize;
                return res;
            }
            catch (Exception ex)
            {
                res.Message = ex.Message;
                res.StatusCode = StatusCodeResponse.EXEPTION;
                return res;
            }
        }
    }
}