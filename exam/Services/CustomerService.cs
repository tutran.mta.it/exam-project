﻿using exam.Core;
using exam.Migrations;
using exam.Models;
using exam.Models.DTO;
using Microsoft.EntityFrameworkCore;

namespace exam.Services
{
    public interface ICustomerService
    {
        public Task<ApiResponse<List<CustomerModel>>> GetListCustomer(CustomerReq model);
    }

    public class CustomerService : ICustomerService
    {
        private readonly MigrationDataContext _dbService;

        public CustomerService(MigrationDataContext dbService)
        {
            _dbService = dbService;
        }

        public async Task<ApiResponse<List<CustomerModel>>> GetListCustomer(CustomerReq model)
        {
            var res = new ApiResponse<List<CustomerModel>>
            {
                Data = new List<CustomerModel>(),
                Message = string.Empty,
                StatusCode = StatusCodeResponse.SUCCESS
            };
            res.Data = await _dbService.Customer
                //.Where(x => string.IsNullOrEmpty(model.CustomerName) || x.Name.Contains(model.CustomerName))
                .Skip((model.PageNumber - 1) * model.PageSize)
                .Take(model.PageSize)
                .ToListAsync();
            return res;
        }
    }
}