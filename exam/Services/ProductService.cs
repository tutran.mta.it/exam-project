﻿using exam.Core;
using exam.Migrations;
using exam.Models;
using exam.Models.DTO;
using Microsoft.EntityFrameworkCore;

namespace exam.Services
{
    public interface IProductService
    {
        public Task<ApiResponse<List<ProductModel>>> GetListProduct(ProductDTOReq model);
    }

    public class ProductService : IProductService
    {
        private readonly MigrationDataContext _dbService;

        public ProductService(MigrationDataContext dbService)
        {
            _dbService = dbService;
        }

        public async Task<ApiResponse<List<ProductModel>>> GetListProduct(ProductDTOReq model)
        {
            var res = new ApiResponse<List<ProductModel>>
            {
                Data = new List<ProductModel>(),
                Message = string.Empty,
                StatusCode = StatusCodeResponse.SUCCESS
            };
            res.Data = await _dbService.Product
                .Where(x => string.IsNullOrEmpty(model.ProductName) || x.Name.Contains(model.ProductName))
                .Skip((model.PageNumber - 1) * model.PageSize)
                .Take(model.PageSize)
                .ToListAsync();
            return res;
        }
    }
}