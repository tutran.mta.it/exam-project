﻿using exam.Core;
using exam.Migrations;
using exam.Models.DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace exam.Services
{
    public interface IAccountService
    {
        public Task<IdentityResult> SignUpAsync(SignUpModel model);

        public Task<ApiResponse<SignInModelRes>> SignInAsync(SignInModel model);

        public Task<ApiResponse<ProfileModelRes>> ProfileAsync(ProfileModelReq model);
    }

    public class AccountService : IAccountService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IConfiguration configuration;

        public AccountService(UserManager<ApplicationUser> userManager
            , SignInManager<ApplicationUser> signInManager
            , IConfiguration configuration)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
        }

        public async Task<ApiResponse<ProfileModelRes>> ProfileAsync(ProfileModelReq model)
        {
            var info = await signInManager.UserManager.FindByEmailAsync(model.Email);
            var res = new ApiResponse<ProfileModelRes>
            {
                Data = new ProfileModelRes
                {
                    Email = info.Email,
                    LastName = info.LastName,
                    FirstName = info.FirstName,
                    PhoneNumber = info.PhoneNumber,
                    UserName = info.UserName
                },
                Message = string.Empty,
                StatusCode = StatusCodeResponse.SUCCESS
            };
            return res;
        }

        public async Task<ApiResponse<SignInModelRes>> SignInAsync(SignInModel model)
        {
            var res = new ApiResponse<SignInModelRes>
            {
                Data = new SignInModelRes(),
                Message = string.Empty,
                StatusCode = StatusCodeResponse.SUCCESS
            };
            var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
            if (!result.Succeeded)
            {
                res.StatusCode = StatusCodeResponse.SIGNUP_FAIL;
                return res;
            }
            var authClaim = new List<Claim>
            {
                new Claim(ClaimTypes.Email,model.Email),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
            };
            var authenKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["AppSettings:JWT:SecretKey"]));
            var token = new JwtSecurityToken
            (
                issuer: configuration["AppSettings:JWT:ValidIssuer"],
                audience: configuration["AppSettings:JWT:ValidAudience"],
                expires: DateTime.Now.AddMinutes(Convert.ToInt32(configuration["AppSettings:JWT:Expired"])),
                claims: authClaim,
                signingCredentials: new SigningCredentials(authenKey, SecurityAlgorithms.HmacSha256)
            );
            var info = await signInManager.UserManager.FindByEmailAsync(model.Email);
            res.Data.AccessToken = new JwtSecurityTokenHandler().WriteToken(token);
            res.Data.FullName = $"{info.FirstName} {info.LastName}";
            res.Data.Email = info.Email;
            return res;
        }

        public async Task<IdentityResult> SignUpAsync(SignUpModel model)
        {
            var user = new ApplicationUser
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = model.Email
            };
            return await userManager.CreateAsync(user, model.Password);
        }
    }
}