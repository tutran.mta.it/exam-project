﻿function Sort(column) {
  var dataResNew = dataRes.sort(function (a, b) {
    if (column === "PRODDUCT") {
      return a.productName > b.productName ? 1 : -1;
    }
    if (column === "CATEGORY") {
      return a.categoryName > b.categoryName ? 1 : -1;
    }
    if (column === "CUSTOMER") {
      return a.customerName > b.customerName ? 1 : -1;
    }
  });
  MapData(dataResNew);
}

function MapData(data) {
  let strBody = "";
  data.forEach((item) => {
    strBody += `
            <tr>
                <td>${item.productName}</td>
                <td>${item.categoryName}</td>
                <td>${item.customerName}</td>
                <td>${item.orderDate}</td>
                <td>${item.amount}</td>
            </tr>`;
  });
  strBody = `
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th onclick="Sort('PRODDUCT')">Product name</th>
                    <th onclick="Sort('CATEGORY')">Category name</th>
                    <th onclick="Sort('CUSTOMER')">Customer name</th>
                    <th>Order date</th>
                    <th>Amount</th>
                </tr>
            </thead>
                <tbody>
                    ${strBody}
                </tbody>
            </table>`;
  $("#list-order").html(strBody);
}

function GenPaging(currentPage, totalPage) {
  if (totalPage === 0) {
    return;
  }
  let previous = `<li class="page-item"><a class="page-link" href="#" aria-label="Previous" onclick="Previous()"><span aria-hidden="true">&laquo;</span></a></li>`;
  let next = `<li class="page-item"><a class="page-link" href="#" aria-label="Next" onclick="Next()"><span aria-hidden="true">&raquo;</span></a></li>`;
  let pagingStrPrevious = `<nav aria-label="Page navigation example"><ul class="pagination justify-content-end">`;
  let pagingStrNext = `</ul></nav>`;
  let pagingStrFirst = "";
  let pagingStrLast = "";
  let pageStr = "";
  for (let i = 1; i <= totalPage; i++) {
    if (currentPage === i) {
      pageStr += `<li class="page-item page-item-${i} active"><a class="page-link" href="#" onclick="JumpPage(${i})">${i}</a></li>`;
    } else {
      pageStr += `<li class="page-item page-item-${i}"><a class="page-link" href="#" onclick="JumpPage(${i})">${i}</a></li>`;
    }
  }
  if (currentPage > 1 && currentPage < totalPage && totalPage !== 1) {
    pageStr = previous + pageStr;
    pageStr = pageStr + next;
  }
  if (currentPage === 1 && totalPage !== 1) {
    pageStr = pageStr + next;
  }
  if (currentPage === totalPage && totalPage !== 1) {
    pageStr = previous + pageStr;
  }

  //xu ly first and last
  if (totalPage >= totalValidShowFirstAndLast) {
    // xu ly first
    if (currentPage - 1 >= jumpStep) {
      pagingStrFirst = `<li class="page-item"><a class="page-link" href="#" onclick="JumpPage(0,'FIRST')">First</a></li>`;
    } else {
      pagingStrFirst = ``;
    }
    // xu ly last
    if (totalPage - currentPage >= jumpStep) {
      pagingStrLast = `<li class="page-item"><a class="page-link" href="#" onclick="JumpPage(1,'LAST')">Last</a></li>`;
    } else {
      pagingStrLast = ``;
    }
  }

  if (totalPage !== 1) {
    $("#paging").html(
      pagingStrPrevious +
        pagingStrFirst +
        pageStr +
        pagingStrLast +
        pagingStrNext
    );
  } else {
    $("#paging").html("");
  }
}

function JumpPage(currentPage, type = "") {
  if (type === "FIRST") {
    currentPage = 1;
  } else if (type === "LAST") {
    currentPage = totalPage;
  }
  ListOrder(textSearchGlobal, currentPage);
  currentPageGlobal = currentPage;
}

function Previous() {
  currentPageGlobal -= 1;
  ListOrder(textSearchGlobal, currentPageGlobal);
}

function Next() {
  currentPageGlobal += 1;
  ListOrder(textSearchGlobal, currentPageGlobal);
}

function CreateOrderAsync(data) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../api/baseorder/submit",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: data,
      headers: requestHeader,
      success: function (data) {
        resolve(data);
      },
      error: function (error) {
        reject(error);
      },
    });
  });
}

function GetListOrder(data) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "./api/baseorder/getlistorder",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: data,
      headers: requestHeader,
      success: function (data) {
        resolve(data);
      },
      error: function (error) {
        reject(error);
      },
    });
  });
}

function CreateOrder() {
  const data = {
    OrderName: $("#orderName").val(),
    Product: $("#product").val(),
    Customer: $("#customer").val(),
    OrderDate: $("#orderDate").val(),
    Amount: $("#amount").val(),
  };
  CreateOrderAsync(JSON.stringify(data))
    .then((res) => {
      if (res.statusCode == "000000") {
        alert("Tạo đơn hàng thành công");
      } else {
        console.log(res);
      }
    })
    .catch((error) => {
      if (error.status === 401) {
        redirectToLogin();
      }
    });
}

function SearchCategory() {
  textSearchGlobal = $("#textSearch").val();
  ListOrder(textSearchGlobal);
}
