﻿function GetListProduct(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '../api/baseproduct/listproduct',
            type: 'POST',
            dataType: 'json',
            contentType: "application/json",
            data: data,
            headers: requestHeader,
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}