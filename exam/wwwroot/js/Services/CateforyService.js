﻿function GetListCategory() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: 'category/categories',
            type: 'POST',
            dataType: 'json',
            contentType: "application/json",
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}