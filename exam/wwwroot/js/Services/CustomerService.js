﻿function SignUpSystem(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: './api/baseaccount/signup',
            type: 'POST',
            dataType: 'json',
            contentType: "application/json",
            data: data,
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}

function SignInSystem(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: './api/baseaccount/signin',
            type: 'POST',
            dataType: 'json',
            contentType: "application/json",
            data: data,
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}

function ProfileAsync(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: './api/baseaccount/profile',
            type: 'POST',
            dataType: 'json',
            contentType: "application/json",
            data: data,
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}

function Login() {
    let data = {
        email: $("#email").val(),
        password: $("#password").val()
    };
    SignInSystem(JSON.stringify(data)).then(res => {
        setCookie("access_token", res.data.accessToken, 1);
        setSessionStorage(storage_full_name, res.data.fullName);
        setSessionStorage(storage_email, res.data.email);
        redirectToPage("home");
    }).catch(error => {
        alert("Đăng nhập thất bại");
    })
}

function Register() {
    let obj = {
        firstName: $("#firstName").val(),
        lastName: $("#lastName").val(),
        email: $("#email").val(),
        password: $("#password").val(),
    };
    SignUpSystem(JSON.stringify(obj)).then(res => {
        alert("Đăng ký thành công");
    }).catch(error => {
        alert("Đăng ký thất bại");
    });
}

function GetListCustomer(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '../api/basecustomer/listcustomer',
            type: 'POST',
            dataType: 'json',
            contentType: "application/json",
            data: data,
            headers: requestHeader,
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}