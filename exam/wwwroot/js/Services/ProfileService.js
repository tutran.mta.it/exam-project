﻿Init();
function ProfileAsync(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: './api/baseaccount/profile',
            type: 'POST',
            dataType: 'json',
            contentType: "application/json",
            data: data,
            headers: requestHeader,
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}

function Init() {
    const obj = {
        email: getSessionStorage("email")
    }
    ProfileAsync(JSON.stringify(obj)).then(res => {
        $("#profile-body").html(
            `<table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>First name</td>
                        <td>${res.data.firstName}</td>
                    </tr>
                    <tr>
                        <td>Last name</td>
                        <td>${res.data.lastName}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>${res.data.email}</td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td>${res.data.userName}</td>
                    </tr>
                    <tr>
                        <td>Phone number</td>
                        <td>${res.data.phoneNumber}</td>
                    </tr>
                </tbody>
            </table>`
        )
    }).catch(error => {
        console.log(error);
    })
}