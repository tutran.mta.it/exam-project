﻿const fullNameUser = getSessionStorage(storage_full_name);
setMenu();
function setMenu() {
    $("#menusidebar")
        .append(fullNameUser === "" || fullNameUser === null
            ? `<li class="nav-item">
    <a class="nav-link text-dark" onclick="redirectToPage('login')"><strong>Đăng nhập hệ thống</strong></a>
                        </li >`
            : `<li class="nav-item">
    <a class="nav-link text-dark" onclick="redirectToPage('profile')"><strong>${fullNameUser}</a>
                        </li >`);
}