﻿const storage_full_name = "full_name";
const storage_email = "email";
const routing_web = [
  { key: "order", name: "Quản lý đơn hàng", routing: "/order", icon: "" },
  { key: "login", name: "Đăng nhập", routing: "/login", icon: "" },
  { key: "register", name: "Đăng ký", routing: "/register", icon: "" },
  { key: "home", name: "Trang chủ", routing: "/home", icon: "" },
];
