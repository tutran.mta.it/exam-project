﻿namespace exam.Models.DTO
{
    public class ApiResponse<T>
    {
        public T Data { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }
    public class ApiResponse
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }
}