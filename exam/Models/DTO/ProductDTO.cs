﻿namespace exam.Models.DTO
{
    public class ProductDTO
    {
    }

    public class ProductDTOReq
    {
        public string ProductName { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}