﻿using System.ComponentModel.DataAnnotations;

namespace exam.Models.DTO
{
    public class CustomerDTO
    {
    }

    public class CustomerLoginReq
    {
        public string Name { get; set; } = string.Empty;
    }

    public class CustomerLoginRes
    {
        public string AccessToken { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
    }

    public class SignInModel
    {
        [Required, EmailAddress]
        public string Email { get; set; } = string.Empty;

        [Required]
        public string Password { get; set; } = string.Empty;
    }

    public class SignUpModel
    {
        [Required]
        public string FirstName { get; set; } = string.Empty;

        [Required]
        public string LastName { get; set; } = string.Empty;

        [Required, EmailAddress]
        public string Email { get; set; } = string.Empty;

        [Required]
        public string Password { get; set; } = string.Empty;
    }

    public class SignInModelRes
    {
        public string AccessToken { get; set; } = string.Empty;
        public string FullName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
    }

    public class ProfileModelReq
    {
        [Required, EmailAddress]
        public string Email { get; set; } = string.Empty;
    }

    public class ProfileModelRes
    {
        public string Email { get; set; } = string.Empty;
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;
        public string UserName { get; set; } = string.Empty;
    }

    public class CustomerReq
    {
        //public string CustomerName { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}