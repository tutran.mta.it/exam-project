﻿namespace exam.Models.DTO
{
    public class OrderDTO
    {
        public string OrderId { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string CustomerName { get; set; }
        public string OrderDate { get; set; }
        public string Amount { get; set; }
    }

    public class OrderModelRes
    {
        public List<OrderDTO> ListProduct { get; set; }
        public int TotalPage { get; set; }

    }
}