﻿namespace exam.Models.DTO
{
    public class OrderCreateDTO
    {
        public string OrderName { get; set; }
        public string Product { get; set; }
        public string Customer { get; set; }
        public string OrderDate { get; set; }
        public string Amount { get; set; }
    }

    public class GetListOrderReq
    {
        public string TextSearch { get; set; }
        public int PageNum { get; set; }
        public int PageSize { get; set; }
    }
}