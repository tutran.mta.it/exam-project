﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace exam.Models
{
    [Table("category")]
    public class CategoryModel
    {
        [Key]
        public Guid CategoryId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
    }
}