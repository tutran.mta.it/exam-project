﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace exam.Models
{
    [Table("order")]
    public class OrderModel
    {
        [Key]
        public Guid OrderId { get; set; }

        [Required]
        public string Customer { get; set; }

        [Required]
        public string Product { get; set; }

        public double Amount { get; set; }
        public DateTime OrderDate { get; set; }
    }
}