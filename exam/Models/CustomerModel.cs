﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace exam.Models
{
    [Table("customer")]
    public class CustomerModel
    {
        [Key]
        public Guid CustomerId { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
    }
}