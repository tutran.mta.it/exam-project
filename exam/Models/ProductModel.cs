﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace exam.Models
{
    [Table("product")]
    public class ProductModel
    {
        [Key]
        public Guid ProductId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Category { get; set; }

        public double Price { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
    }
}