﻿using exam.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace exam.Migrations
{
    public class MigrationDataContext : IdentityDbContext<ApplicationUser>
    {
        public MigrationDataContext(DbContextOptions<MigrationDataContext> options) : base(options)
        {
        }

        public DbSet<CategoryModel> Category { get; set; }
        public DbSet<CustomerModel> Customer { get; set; }
        public DbSet<OrderModel> Order { get; set; }
        public DbSet<ProductModel> Product { get; set; }
    }
}