﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace exam.Migrations
{
    /// <inheritdoc />
    public partial class updaterelationshipdbexam1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_category_product_ProductModelProductId",
                table: "category");

            migrationBuilder.DropForeignKey(
                name: "FK_customer_order_OrderModelOrderId",
                table: "customer");

            migrationBuilder.DropForeignKey(
                name: "FK_product_order_OrderModelOrderId",
                table: "product");

            migrationBuilder.DropIndex(
                name: "IX_product_OrderModelOrderId",
                table: "product");

            migrationBuilder.DropIndex(
                name: "IX_customer_OrderModelOrderId",
                table: "customer");

            migrationBuilder.DropIndex(
                name: "IX_category_ProductModelProductId",
                table: "category");

            migrationBuilder.DropColumn(
                name: "OrderModelOrderId",
                table: "product");

            migrationBuilder.DropColumn(
                name: "OrderModelOrderId",
                table: "customer");

            migrationBuilder.DropColumn(
                name: "ProductModelProductId",
                table: "category");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "OrderModelOrderId",
                table: "product",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OrderModelOrderId",
                table: "customer",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProductModelProductId",
                table: "category",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_product_OrderModelOrderId",
                table: "product",
                column: "OrderModelOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_customer_OrderModelOrderId",
                table: "customer",
                column: "OrderModelOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_category_ProductModelProductId",
                table: "category",
                column: "ProductModelProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_category_product_ProductModelProductId",
                table: "category",
                column: "ProductModelProductId",
                principalTable: "product",
                principalColumn: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_customer_order_OrderModelOrderId",
                table: "customer",
                column: "OrderModelOrderId",
                principalTable: "order",
                principalColumn: "OrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_product_order_OrderModelOrderId",
                table: "product",
                column: "OrderModelOrderId",
                principalTable: "order",
                principalColumn: "OrderId");
        }
    }
}