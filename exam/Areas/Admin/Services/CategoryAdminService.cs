﻿using exam.Areas.Admin.Models;
using exam.Core;
using exam.Migrations;
using exam.Models.DTO;
using Microsoft.EntityFrameworkCore;

namespace exam.Areas.Admin.Services
{
    public interface ICategoryAdminService
    {
        Task<ApiResponse<List<CategoryModelBase>>> GetListCategory();
    }

    public class CategoryAdminService : ICategoryAdminService
    {
        private readonly MigrationDataContext _dbService;

        public CategoryAdminService(MigrationDataContext dbService)
        {
            _dbService = dbService;
        }

        public async Task<ApiResponse<List<CategoryModelBase>>> GetListCategory()
        {
            var reval = new ApiResponse<List<CategoryModelBase>>
            {
                Data = new List<CategoryModelBase>(),
                Message = "",
                StatusCode = StatusCodeResponse.SUCCESS
            };
            reval.Data = await _dbService.Category.Select(x => new CategoryModelBase
            {
                CategoryId = x.CategoryId.ToString(),
                Name = x.Name,
                Description = x.Description
            }).ToListAsync();
            return reval;
        }
    }
}