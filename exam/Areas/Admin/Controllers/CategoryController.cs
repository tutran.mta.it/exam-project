﻿using exam.Areas.Admin.Models;
using exam.Areas.Admin.Services;
using exam.Models.DTO;
using Microsoft.AspNetCore.Mvc;

namespace exam.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/category")]
    public class CategoryController : Controller
    {
        private readonly ICategoryAdminService _categoryService;

        public CategoryController(ICategoryAdminService categoryService)
        {
            _categoryService = categoryService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("categories")]
        [HttpPost]
        public async Task<ApiResponse<List<CategoryModelBase>>> GetListCategory()
        {
            return await _categoryService.GetListCategory();
        }
    }
}