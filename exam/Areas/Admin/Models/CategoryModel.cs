﻿namespace exam.Areas.Admin.Models
{
    public class CategoryModel
    {
    }

    public class CategoryModelBase
    {
        public string CategoryId { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}
