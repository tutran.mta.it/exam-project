﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace exam.Core
{
    public class ConvertUtility
    {
        private static string DefaultCcyCulture => "en-US";

        private static string CurrencyFormat(string ccy)
        {
            if (ccy == "VND" || ccy == "VNĐ")
                return "#,##0";
            return "#,##0.00";
        }

        public static string FormatAmoutSpecialCcy(double amount, string baseCcy)
        {
            if (baseCcy == "VND")
            {
                amount = Math.Round(amount, MidpointRounding.AwayFromZero);
            }
            string retVal = string.Format("{0} {1}", amount.ToString(CurrencyFormat(baseCcy), new CultureInfo(DefaultCcyCulture)), "VND");
            return retVal;
        }

        public static string UnicodeToKoDau(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;

            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
    }
}