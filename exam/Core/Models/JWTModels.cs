﻿namespace exam.Core.Models
{
    public class JWTModels
    {
    }

    public class AppSettings
    {
        public TokenInformation JWT { get; set; }
    }

    public class TokenInformation
    {
        public int Expired { get; set; }
        public string SecretKey { get; set; }
    }
}