﻿using exam.Core.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace exam.Core.Libs
{
    public interface IJWTService
    {
        string GetJwtToken(string username);
    }

    public class JWTService : IJWTService
    {
        private readonly IOptions<AppSettings> _appSettingModel;

        public JWTService(IOptions<AppSettings> appSettingModel)
        {
            _appSettingModel = appSettingModel;
        }

        public string GetJwtToken(string username)
        {
            var secretKeyByte = Encoding.UTF8.GetBytes(_appSettingModel.Value.JWT.SecretKey);
            var tokenHandler = new JwtSecurityTokenHandler();
            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, username)
                }),
                Expires = now.AddMinutes(Convert.ToInt32(_appSettingModel.Value.JWT.Expired)),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(secretKeyByte),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);
            return token;
        }
    }
}