﻿namespace exam.Core
{
    public class ModuleConstant
    {
    }

    public static class StatusCodeResponse
    {
        public const string SUCCESS = "000000";
        public const string EXEPTION = "999999";

        #region order product

        public const string PRODUCT_INVALID = "ORD001";
        public const string QUANTITY_PRODUCT_INVALID = "ORD002";
        public const string GET_INFOR_CUSTOMER_FAIL = "CUS001";

        #endregion order product

        #region account

        public const string SIGNUP_FAIL = "ACT001";

        #endregion account
    }
}