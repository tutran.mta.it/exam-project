﻿using exam.Models;
using exam.Models.DTO;
using exam.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace exam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public BaseProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost("listproduct")]
        [Authorize]
        public async Task<ApiResponse<List<ProductModel>>> ListProduct(ProductDTOReq model)
        {
            return await _productService.GetListProduct(model);
        }
    }
}