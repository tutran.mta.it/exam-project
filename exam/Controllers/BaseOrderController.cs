﻿using exam.Models.DTO;
using exam.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace exam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BaseOrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly ILogger<BaseOrderController> _logger;

        public BaseOrderController(IOrderService orderService
            , ILogger<BaseOrderController> logger)
        {
            _orderService = orderService;
            _logger = logger;
        }

        [HttpPost]
        [Route("submit")]
        public async Task<ApiResponse<OrderCreateDTO>> Submit([FromBody] OrderCreateDTO instance)
        {
            var res = await _orderService.CreateOrder(instance);
            return res;
        }

        [HttpPost]
        [Route("getlistorder")]
        public async Task<ApiResponse<OrderModelRes>> GetListOrder([FromBody] GetListOrderReq instance)
        {
            var res = await _orderService.GetListOrder(instance.TextSearch, instance.PageNum, instance.PageSize);
            return res;
        }
    }
}