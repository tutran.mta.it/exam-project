﻿using exam.Models;
using exam.Models.DTO;
using exam.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace exam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseCustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public BaseCustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpPost("listcustomer")]
        [Authorize]
        public async Task<ApiResponse<List<CustomerModel>>> ListProduct(CustomerReq model)
        {
            return await _customerService.GetListCustomer(model);
        }
    }
}