﻿using exam.Core;
using exam.Models.DTO;
using exam.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace exam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseAccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly ILogger<BaseAccountController> _logger;

        public BaseAccountController(IAccountService accountService
            , ILogger<BaseAccountController> logger)
        {
            _accountService = accountService;
            _logger = logger;
        }

        [HttpPost("signup")]
        public async Task<ApiResponse> SignUp(SignUpModel model)
        {
            var result = await _accountService.SignUpAsync(model);
            if (result.Succeeded)
            {
                return new ApiResponse
                {
                    StatusCode = StatusCodeResponse.SUCCESS,
                    Message = string.Empty
                };
            }
            return new ApiResponse
            {
                StatusCode = StatusCodeResponse.SIGNUP_FAIL,
                Message = string.Empty
            };
        }

        [HttpPost("signin")]
        public async Task<ApiResponse<SignInModelRes>> SignIn(SignInModel model)
        {
            return await _accountService.SignInAsync(model);
        }

        [HttpPost("profile")]
        [Authorize]
        public async Task<ApiResponse<ProfileModelRes>> Profile(ProfileModelReq model)
        {
            return await _accountService.ProfileAsync(model);
        }
    }
}