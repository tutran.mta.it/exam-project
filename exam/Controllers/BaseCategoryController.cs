﻿using exam.Services;
using Microsoft.AspNetCore.Mvc;

namespace exam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseCategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public BaseCategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
    }
}